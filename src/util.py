import asyncio
import functools
import time
from typing import Any, Callable


def async_timed():
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        async def wrapped(*args, **kwargs) -> Any:
            print(f"starging {func} with args {args} {kwargs}")
            start = time.time()
            try:
                return await func(*args, **kwargs)
            finally:
                end = time.time()
                total = end - start
                print(f"finished {func} in {total:.4f} seconds")

        return wrapped

    return wrapper


@async_timed()
async def delay(delay_seconds: int) -> int:
    print(f"sleeping for {delay_seconds} seconds")
    await asyncio.sleep(delay_seconds)
    print(f"finishing sleeping for {delay_seconds} seconds")
    return delay_seconds


@async_timed()
async def calculate(count: int) -> int:
    print(f"calculate count = {count}")
    total = 0
    for i in range(count):
        total += 1
    print(f"finishing calculating for count = {count}")
    return total
