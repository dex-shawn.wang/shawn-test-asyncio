import asyncio
from src.util import calculate, delay, async_timed


@async_timed()
async def basic_async_1():
    coroutine_delay = delay(1)
    result = await coroutine_delay
    print(result)


@async_timed()
async def basic_async_2():
    delay_one_second_1 = delay(1)
    delay_one_second_2 = delay(1)
    await delay_one_second_1
    await delay_one_second_2


@async_timed()
async def basic_async_3():
    delay_one_second_1 = asyncio.create_task(delay(1))
    delay_one_second_2 = asyncio.create_task(delay(1))
    await delay_one_second_1
    await delay_one_second_2


@async_timed()
async def basic_wait_for():
    long_task = asyncio.create_task(delay(1))
    # long_task = asyncio.create_task(calculate(100000000))
    try:
        await asyncio.wait_for(long_task, 0.5)
    except asyncio.TimeoutError:
        print("got a timeout")
        print(f"long_task finished {long_task.done()}")


def test_finally():
    try:
        return 1
    finally:
        print("in finally")


if __name__ == "__main__":
    # asyncio.run(basic_async_1())
    # asyncio.run(basic_async_2())
    # asyncio.run(basic_async_3())
    # asyncio.run(basic_wait_for())
    print(test_finally())
