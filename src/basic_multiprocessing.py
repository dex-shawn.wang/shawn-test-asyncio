import multiprocessing
from multiprocessing import Pool
import time


def count(count_to: int) -> float:
    start = time.time()
    result = 0
    for counter in range(count_to):
        result += 1
    end = time.time()
    print(f"count to {count_to} duration {end-start} s")
    return end - start


def process_pool():
    start = time.time()
    with Pool() as process_pool:
        resutl_1 = process_pool.apply(count, args=(50000000,))
        result_2 = process_pool.apply(count, args=(100000000,))
        print(f"result_1 {resutl_1}")
        print(f"result_2 {result_2}")
    end = time.time()
    print(f"total duration {end-start} s")


def process_pool_async():
    start = time.time()
    with Pool() as process_pool:
        resutl_1 = process_pool.apply_async(count, args=(100000000,))
        result_2 = process_pool.apply_async(count, args=(50000000,))
        print(f"result_1 {resutl_1.get()}")
        print(f"result_2 {result_2.get()}")
    end = time.time()
    print(f"total duration {end-start} s")


def time_process_pool_async():
    start = time.time()
    with Pool() as process_pool:
        duration = process_pool.apply_async(count, args=(100000000,))
        duration = duration.get()
        print(f"result_1 {duration}")
    end = time.time()
    total_duration = end - start
    print(f"total duration {total_duration} s")
    print(f"overhead {total_duration - duration} s")


if __name__ == "__main__":
    # process_pool()
    # process_pool_async()
    time_process_pool_async()
