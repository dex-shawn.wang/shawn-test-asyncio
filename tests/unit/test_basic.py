import asyncio
import unittest
from src.basic_async import basic_async_1, basic_async_2, basic_async_3


class TestBasic(unittest.TestCase):
    def test_basic(self):
        asyncio.run(basic_async_1())

    def test_basic(self):
        asyncio.run(basic_async_2())

    def test_basic(self):
        asyncio.run(basic_async_3())
